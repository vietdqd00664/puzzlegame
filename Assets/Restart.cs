﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Restart : MonoBehaviour
{
    public GameObject check;
    public void LoadScene(string sceneName)
    {
        SceneManager.LoadScene(sceneName);
        DropSlot.isWin = false;
        check.SetActive(false);
        DragHandler.num = 0;

    }
}
