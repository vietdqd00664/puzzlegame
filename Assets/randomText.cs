﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class randomText : MonoBehaviour
{
    // Start is called before the first frame update
    public Text GameChat;
    private int message = 0;

    void Start()
    {
        InvokeRepeating("Chat", 0, 9999999);
        message = Random.Range(1, 5);
    }
    void Chat()
    {
        if (message == 1)
            GameChat.text = "Text 1";
        else if (message == 2)
            GameChat.text = "Text 2";
        else if (message == 3)
            GameChat.text = "Text 3";
        else if (message == 4)
            GameChat.text = "Text 4";
        else if (message == 5)
            GameChat.text = "Text 5";
    }

}
