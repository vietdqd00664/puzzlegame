﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hand : MonoBehaviour
{
	public DragHandler pair;
	public float ratio;
	void Update()
	{
		transform.position = pair.transform.position + (pair.target.transform.position - pair.transform.position) * ratio;
	}
}
