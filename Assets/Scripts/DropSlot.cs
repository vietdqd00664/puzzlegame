﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class DropSlot : MonoBehaviour, IDropHandler
{
	private GameObject item;
	public GameObject Drop;
	public static bool isWin;

	public void OnDrop(PointerEventData eventData)
	{
		if (!item)
		{
			item = DragHandler.itemDragging;
			if (item.name == Drop.name)
			{
                GameObject.FindGameObjectWithTag("TrueAudio").GetComponent<AudioSource>().Play();
                item.tag = "Untagged";
				Drop.tag = "Lock";
				item.transform.SetParent(transform);
				item.transform.position = transform.position;
				DragHandler.num += 1;
				Debug.Log(" number:" + DragHandler.num);
				if (DragHandler.num >= GameObject.FindGameObjectsWithTag("Slot").Length)
				{
					isWin = true;					
				}
            }
            else
            {                
                GameObject.FindGameObjectWithTag("FalseAudio").GetComponent<AudioSource>().Play();
            }
		}
	}

	void Update()
	{
		if (item != null && item.transform.parent != transform)
		{
			item = null;
		}
	}
}
