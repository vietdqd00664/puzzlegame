﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using System;

public class DragHandler : MonoBehaviour, IDragHandler, IEndDragHandler, IBeginDragHandler
{
	public static GameObject itemDragging;
	public static Vector3 startPosition;
	Transform startParent, dragParent;
	public Transform target;
	public static int num = 0;
	public static bool isPlayHand, isTutorialFinish;
    public AudioClip DragAudio;
    private AudioSource Audio;
    public GameObject[] List;


    void Start()
	{
        Audio = GetComponent<AudioSource>();
        dragParent = GameObject.FindGameObjectWithTag("DragParent").transform;
		InvokeRepeating("RepeatTutorial", 0, 5);
		StartCoroutine(Wait());
		IEnumerator Wait()
		{
			yield return new WaitForSeconds(1);
			isPlayHand = false;
		}
	}

	public void OnBeginDrag(PointerEventData eventData)
	{
        Audio.clip = DragAudio;
        Audio.Play();
		itemDragging = gameObject;
		startPosition = transform.position;
		startParent = transform.parent;
		transform.SetParent(dragParent);
		isPlayHand = false;
		isTutorialFinish = true;	
	}

	public void OnDrag(PointerEventData eventData)
	{
		transform.position = Input.mousePosition;
	}

	public void OnEndDrag(PointerEventData eventData)
	{
		itemDragging = null;
		if (transform.parent == dragParent)
		{
			transform.position = startPosition;
			transform.SetParent(startParent);
		}
        List = GameObject.FindGameObjectsWithTag("Pool");
        if ( List.Length>=1) {
            //StartCoroutine(Wait());

            StartCoroutine(StartCountdown());
            float currCountdownValue;
            IEnumerator StartCountdown(float countdownValue = 5)
            {
                currCountdownValue = countdownValue;
                while (currCountdownValue > 0)
                {
                    yield return new WaitForSeconds(1.0f);
                    currCountdownValue--;

                    if (currCountdownValue <= 0 && itemDragging == null )
                    {
                        int ran = UnityEngine.Random.Range(0, List.Length);
                        Debug.Log("---" + List[ran]);
                        List[ran].transform.localScale = new Vector2(1.1f, 1.1f);
                        yield return new WaitForSeconds(2);
                        List[ran].transform.localScale = new Vector2(1, 1);
                    }
                    else if (itemDragging)
                    {
                        currCountdownValue = 100;
                       
                    }
                }

            }


        //    IEnumerator Wait()
        //{
        //    yield return new WaitForSeconds(5);
        //    int ran = UnityEngine.Random.Range(0, List.Length);
        //    Debug.Log("---" + List[ran]);
        //    List[ran].transform.localScale = new Vector2(1.1f, 1.1f);
        //    yield return new WaitForSeconds(2);
        //    List[ran].transform.localScale = new Vector2(1, 1);
        //}
        }
    }

	void RepeatTutorial()
	{
		if (!isTutorialFinish)
		{
			isPlayHand = true;
			StartCoroutine(Wait());
			IEnumerator Wait()
			{
				yield return new WaitForSeconds(1);
				isPlayHand = false;
			}
		}
	}
}