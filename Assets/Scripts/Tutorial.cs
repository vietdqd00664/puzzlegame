﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Tutorial : MonoBehaviour
{
	public DragHandler[] pairs;
	public Hand hand;

	public void Start()
	{
		int randomNumber = Random.Range(0, pairs.Length);
		var pair = pairs[randomNumber];
		hand.pair = pair;
	}
}