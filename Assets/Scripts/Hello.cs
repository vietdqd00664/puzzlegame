﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hello : MonoBehaviour
{
	private AudioSource audiosource, crashSource, winSource;
	public AudioClip AudioClip, CrashAudio, WinAudio;
	public GameObject MainImage, Pool, PogoHello, Slot, Hand, MainImageClone, CheckButton;
	private float duration, winDuration, crashDuration;

	// Start is called before the first frame update
	void Start()
	{
       
		audiosource = GetComponent<AudioSource>();
		audiosource.clip = AudioClip;
		audiosource.Play();
		duration = AudioClip.length;
		StartCoroutine(WaitForSound());
	}

	IEnumerator WaitForSound()
	{
		yield return new WaitForSeconds(duration);
		MainImage.SetActive(true);
		PogoHello.SetActive(false);
		crashSource = GetComponent<AudioSource>();
		crashSource.clip = CrashAudio;
		crashSource.Play();

		yield return new WaitForSeconds(crashDuration);
		MainImage.SetActive(false);
		Pool.SetActive(true);
		Slot.SetActive(true);
		DragHandler.isPlayHand = true;
	}

	private void Update()
	{
       
        if (DragHandler.isPlayHand)
		{
			Hand.SetActive(true);
		}
		else
		{
			Hand.SetActive(false);
		}
		if (DropSlot.isWin)
		{
			winSource = GetComponent<AudioSource>();
			winSource.clip = WinAudio;
			winSource.Play();
			StartCoroutine(WaitSoundWin());

			IEnumerator WaitSoundWin()
			{
				yield return new WaitForSeconds(winDuration);
				Slot.SetActive(false);
				MainImageClone.SetActive(true);
				CheckButton.SetActive(true);
			}
		}
	}
}
