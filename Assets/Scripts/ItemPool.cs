﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class ItemPool : MonoBehaviour, IDropHandler
{
	void Start()
	{
		for (int i = 0; i < GameObject.FindGameObjectsWithTag("Pool").Length; i++)
		{
			transform.GetChild(i).SetSiblingIndex(Random.Range(0, GameObject.FindGameObjectsWithTag("Pool").Length - 1));
		}
	}

	public void OnDrop(PointerEventData eventData)
	{
		DragHandler.itemDragging.transform.SetParent(transform);
		Debug.Log(" number:" + DragHandler.num);
	}
}

