﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Button : MonoBehaviour
{
    public GameObject Closing ;
    public static bool isRestart;
	public void BackButton()
	{
		SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex - 1);
	}
    public void CheckButton()
    {
        Closing.SetActive(true);
    }

    public void ResetButton()
    {
        isRestart = true;
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
      
        DragHandler.num = 0;

      
    }

    public void NextButton()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }
   

}
